#! /bin/sh

echo '* Updating the package repository information'
echo
apt-get update
echo
echo '* Installing sudo'
echo
apt-get install sudo
echo
echo '* Updating the package repository information'
echo
sudo apt-get update && sudo apt-get upgrade
echo
echo '* Installing curl'
echo
sudo apt-get install curl
echo
host=$1
if [ -z "$1" ]
    then
        host=$(hostname -f)
fi
echo '* Set hostname' $host
echo
sudo hostnamectl set-hostname $host
echo
echo '* Downloading wikisuite installer'
echo
curl -o wikisuite-installer https://gitlab.com/wikisuite/wikisuite-packages/-/raw/main/wikisuite-installer
echo
echo '* Execute wikisuite-installer bash'
sudo bash wikisuite-installer
echo